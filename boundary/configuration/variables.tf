variable "boundary_address" {
  description = "Boundary external IP"
  type        = string
#   default     = "example"
}

variable "auth_method_id" {
  description = "Boundary auth method ID"
  type        = string
  default     = "ampw_1234567890"
}

variable "password_auth_method_login_name" {
  description = "Boundary password auth method login name"
  type        = string
  default     = "admin"
}

variable "password_auth_method_password" {
  description = "Boundary password auth method password"
  type        = string
  default     = "password"
}

# variable "backend_ips" {
#   description = "Backend server's IPs"
#   type        = set(string)
#   default     = ["10.10.10.24", "10.0.0.5",]
# }
