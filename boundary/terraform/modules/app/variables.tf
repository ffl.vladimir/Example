variable "public_key_path" {
  description = "Path to the public key used for ssh access"
}

variable "vm_image" {
  description = "Disk image for reddit db"
  type        = string
  default     = "fd83klic6c8gfgi40urb"
}

variable "ssh_user" {
  description = "User for ssh connection"
}

variable "vmname" {
  description = "VM name"
  type        = string
}

variable zone {
  description = "Zone"
  # Значение по умолчанию
  default = "ru-central1-a"
}
variable subnet_id {
  description = "Subnet"
}
