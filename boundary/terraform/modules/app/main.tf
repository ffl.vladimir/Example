terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.61.0"
    }
  }
}

data "yandex_vpc_network" "cluster" {
  name = "cluster-bitrix"
}

data "yandex_vpc_route_table" "route-out-via-bastion" {
  name = "route-out-via-bastion"
}

data "yandex_vpc_subnet" "cluster_ru_central1_a" {
  name = "cluster-bitrix-ru-central1-a"
}

resource "yandex_compute_instance" "app" {
  name        = var.vmname
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  service_account_id = "ajec5m8is88ej139ksid"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      size     = 20
      type     = "network-hdd"
      image_id = var.vm_image
    }
  }

  network_interface {
    subnet_id = "${data.yandex_vpc_subnet.cluster_ru_central1_a.id}"
    nat = true
  }  


  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.public_key_path)}"
  }
}
