variable "cloud_id" {
  description = "Cloud"
}

# variable "folder_id" {
#   description = "Folder"
# }

variable "zone" {
  description = "Zone"
  default     = "ru-central1-a"
}

variable "public_key_path" {
  description = "Path to the public key used for ssh access"
}

variable "ssh_user" {
  description = "User for ssh connection"
}
variable "service_account_key_file" {
  description = "bitrix.json"
}

variable "vmname" {
  description = "VM name"
  type        = string
}
variable "cloud_folder_id" {
  description = "Folder id in Yandex cloud that contains our cluster"
  type        = string
}

# variable "subnet_id" {
#   description = "Subnets for modules"
# }