terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.61.0"
    }
  }
}

data "yandex_vpc_network" "cluster" {
  name = "cluster-bitrix"
}

data "yandex_vpc_route_table" "route-out-via-bastion" {
  name = "route-out-via-bastion"
}

data "yandex_vpc_subnet" "cluster_ru_central1_a" {
  name = "cluster-bitrix-ru-central1-a"
}

provider "yandex" {
  service_account_key_file = var.service_account_key_file
  cloud_id                 = var.cloud_id
  folder_id                = var.cloud_folder_id
}

module "app" {
  source                = "./modules/app"
  vmname                = var.vmname
  public_key_path       = var.public_key_path
  ssh_user              = var.ssh_user
  subnet_id             = "${data.yandex_vpc_subnet.cluster_ru_central1_a.id}"
}